package mapandin.yonathan.latihanappx0a

import android.net.Uri
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDataMhs (val dataMhs : List<HashMap<String,String>>) :
    RecyclerView.Adapter<AdapterDataMhs.HolderDataMhs>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterDataMhs.HolderDataMhs {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_mhs,parent,false)
        return HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return dataMhs.size
    }

    override fun onBindViewHolder(holder: AdapterDataMhs.HolderDataMhs, position: Int) {
        val data = dataMhs.get(position)
        holder.txNim.setText(data.get("nim"))
        holder.txNama.setText(data.get("nama"))
        holder.txProdi.setText(data.get("nama_prodi"))
        holder.txAlamat.setText(data.get("alamat"))
        holder.txJenisKelamin.setText(data.get("jenis_kelamin"))

        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.photo);
    }

    class HolderDataMhs(v : View) : RecyclerView.ViewHolder(v){
        val txNim = v.findViewById<TextView>(R.id.txNim)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txProdi = v.findViewById<TextView>(R.id.txProdi)
        val txAlamat = v.findViewById<TextView>(R.id.txAlamat)
        val txJenisKelamin = v.findViewById<TextView>(R.id.txJenisKelamin)
        val photo = v.findViewById<ImageView>(R.id.imageView)
    }
}